package com.bapemployeemanagement.capstoneproject.controller;

import com.bapemployeemanagement.capstoneproject.entity.Employee;
import com.bapemployeemanagement.capstoneproject.service.EmployeeService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
public class BaseController {
    private final EmployeeService employeeService;

    public BaseController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    public String getSiteURL(HttpServletRequest request) {
        String siteURL = request.getRequestURL().toString();
        return siteURL.replace(request.getServletPath(), "");
    }

    public Employee getEmployee(Principal principal) {
        User user = (User) ((Authentication) principal).getPrincipal();
        String email = user.getUsername();
        return employeeService.findEmployeeByEmail(email);
    }
}
