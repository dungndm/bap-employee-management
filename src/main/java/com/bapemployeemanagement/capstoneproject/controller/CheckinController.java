package com.bapemployeemanagement.capstoneproject.controller;

import com.bapemployeemanagement.capstoneproject.entity.Attendance;
import com.bapemployeemanagement.capstoneproject.entity.Employee;
import com.bapemployeemanagement.capstoneproject.entity.dto.AttendanceDto;
import com.bapemployeemanagement.capstoneproject.service.AttendanceService;
import com.bapemployeemanagement.capstoneproject.service.EmployeeService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class CheckinController extends BaseController {
    /**
     * URLs
     */
    private static final String CHECK_IN_URL = "/user/check-in";
    private static final String CHECK_IN_SUCCESSFUL_URL = "/user/check-in-successful";
    private static final String CHECK_OUT_URL = "/user/check-out";
    private static final String CHECK_OUT_SUCCESSFUL_URL = "/user/check-out-successful";
    private static final String LIST_ATTENDANCE_URL = "user/list-attendance";

    /**
     * Templates
     */
    private static final String CHECK_IN_PAGE_TEMPLATE = "check_in";
    private static final String CHECK_OUT_PAGE_TEMPLATE = "check_out";
    private static final String ATTENDANCE_INFO_PAGE_TEMPLATE = "attendance_info";

    private final AttendanceService attendanceService;

    public CheckinController(EmployeeService employeeService, AttendanceService attendanceService) {
        super(employeeService);
        this.attendanceService = attendanceService;
    }

    @GetMapping(CHECK_IN_URL)
    public String checkIn(Model model, Principal principal) {
        Employee employee = getEmployee(principal);
        AttendanceDto attendanceDto = new AttendanceDto();
        attendanceDto.setEmployee(employee);
        model.addAttribute("attendanceDto", attendanceDto);
        return CHECK_IN_PAGE_TEMPLATE;
    }

    @PostMapping(CHECK_IN_SUCCESSFUL_URL)
    public String checkInSuccessful(@ModelAttribute("attendanceDto") AttendanceDto attendanceDto,
                                    Model model) {
        Attendance attendance = new Attendance();
        BeanUtils.copyProperties(attendanceDto, attendance);
        Date date = new Date();
        attendance.setCheckIn(date);
        attendanceService.saveAttendance(attendance);
        model.addAttribute("attendance", attendance);
        List<Attendance> attendanceList = attendanceService.getAllAttendance(attendance.getEmployee().getEmployeeId());
        model.addAttribute("attendanceList", attendanceList);
        return "redirect:/" + LIST_ATTENDANCE_URL;
    }

    @GetMapping(CHECK_OUT_URL)
    public String checkOut(Model model) {
        Attendance attendance = attendanceService.getLatestAttendance();
        AttendanceDto attendanceDto = new AttendanceDto();
        BeanUtils.copyProperties(attendance, attendanceDto);
        Date checkIn = attendance.getCheckIn();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String checkInString = dateFormat.format(checkIn);
        attendanceDto.setCheckIn(checkInString);
        model.addAttribute("attendanceDto", attendanceDto);
        return CHECK_OUT_PAGE_TEMPLATE;
    }

    @PostMapping(CHECK_OUT_SUCCESSFUL_URL)
    public String checkOutSuccessful(@ModelAttribute("attendanceDto") AttendanceDto attendanceDto,
                                     Model model) throws ParseException {
        Attendance attendance = new Attendance();
        BeanUtils.copyProperties(attendanceDto, attendance);
        String checkInString = attendanceDto.getCheckIn();
        Date checkIn = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(checkInString);
        attendance.setCheckIn(checkIn);
        Date date = new Date();
        attendance.setCheckOut(date);
        attendanceService.saveAttendance(attendance);
        List<Attendance> attendanceList = attendanceService.getAllAttendance(attendance.getEmployee().getEmployeeId());
        model.addAttribute("attendanceList", attendanceList);
        return "redirect:/" + LIST_ATTENDANCE_URL;
    }

    @GetMapping(LIST_ATTENDANCE_URL)
    public String listAttendance(Model model, Principal principal) {
        Employee employee = getEmployee(principal);
        List<Attendance> attendanceList = attendanceService.getAllAttendance(employee.getEmployeeId());
        model.addAttribute("attendanceList", attendanceList);
        return ATTENDANCE_INFO_PAGE_TEMPLATE;
    }
}
