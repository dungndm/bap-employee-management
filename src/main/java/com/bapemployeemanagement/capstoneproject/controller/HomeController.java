package com.bapemployeemanagement.capstoneproject.controller;

import com.bapemployeemanagement.capstoneproject.entity.Employee;
import com.bapemployeemanagement.capstoneproject.service.EmployeeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class HomeController extends BaseController {
    /**
     * URLs
     */
    private static final String WELCOME_URL = "/";
    private static final String ADMIN_URL = "/admin";
    private static final String USER_URL = "/user";
    private static final String LOGIN_URL = "/login";
    private static final String LOGIN_ERROR_URL = "/login-error";
    private static final String LOGIN_SUCCESSFUL_URL = "/logoutSuccessful";

    /**
     * Templates
     */
    private static final String WELCOME_PAGE_TEMPLATE = "welcome_page";
    private static final String ADMIN_PAGE_TEMPLATE = "admin_page";
    private static final String USER_PAGE_TEMPLATE = "user_page";
    private static final String LOGIN_PAGE_TEMPLATE = "login_page";

    public HomeController(EmployeeService employeeService) {
        super(employeeService);
    }

    @GetMapping(WELCOME_URL)
    public String welcomePage() {
        return WELCOME_PAGE_TEMPLATE;
    }

    @GetMapping(ADMIN_URL)
    public String adminPage(Model model, Principal principal) {
        Employee employee = getEmployee(principal);
        model.addAttribute("employee", employee);
        return ADMIN_PAGE_TEMPLATE;
    }

    @GetMapping(USER_URL)
    public String userPage(Model model, Principal principal) {
        Employee employee = getEmployee(principal);
        model.addAttribute("employee", employee);
        return USER_PAGE_TEMPLATE;
    }

    @GetMapping(LOGIN_URL)
    public String loginPage() {
        return LOGIN_PAGE_TEMPLATE;
    }

    @GetMapping(LOGIN_ERROR_URL)
    public String loginError(Model model) {
        model.addAttribute("hasError", true);
        model.addAttribute("messageError", "Email or password is incorrect");
        return LOGIN_PAGE_TEMPLATE;
    }

    @GetMapping(LOGIN_SUCCESSFUL_URL)
    public String logoutSuccessful() {
        return LOGIN_PAGE_TEMPLATE;
    }
}
