package com.bapemployeemanagement.capstoneproject.controller;

import com.bapemployeemanagement.capstoneproject.entity.Employee;
import com.bapemployeemanagement.capstoneproject.service.EmailService;
import com.bapemployeemanagement.capstoneproject.service.EmployeeService;
import net.bytebuddy.utility.RandomString;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

@Controller
public class ResetPasswordController extends BaseController  {
    /**
     * URLs
     */
    private static final String FORGOT_PASSWORD_URL = "/forgot_password";
    private static final String RESET_PASSWORD_URL = "/reset_password";

    /**
     * Templates
     */
    private static final String FORGOT_PASSWORD_TEMPLATE = "forgot_password_form";
    private static final String RESET_PASSWORD_TEMPLATE = "reset_password_form";
    private static final String MESSAGE_TEMPLATE = "message";

    private final EmailService emailService;
    private final EmployeeService employeeService;

    public ResetPasswordController(EmailService emailService, EmployeeService employeeService) {
        super(employeeService);
        this.emailService = emailService;
        this.employeeService = employeeService;
    }

    @GetMapping(FORGOT_PASSWORD_URL)
    public String showForgotPasswordForm() {
        return FORGOT_PASSWORD_TEMPLATE;
    }

    @PostMapping(FORGOT_PASSWORD_URL)
    public String processForgotPassword(HttpServletRequest request, Model model) {
        String email = request.getParameter("email");
        String token = RandomString.make(30);
        try {
            employeeService.updateResetPasswordToken(token, email);
            String resetPasswordLink = getSiteURL(request) + "/reset_password?token=" + token;
            emailService.sendEmail(email, resetPasswordLink);
            model.addAttribute("message", "We have sent a reset password link to your email. Please check.");
        } catch (UsernameNotFoundException ex) {
            model.addAttribute("error", ex.getMessage());
        } catch (MessagingException | UnsupportedEncodingException e) {
            model.addAttribute("error", "Error while sending email");
        }
        return FORGOT_PASSWORD_TEMPLATE;
    }

    @GetMapping(RESET_PASSWORD_URL)
    public String showResetPasswordForm(@Param(value = "token") String token, Model model) {
        Employee employee = employeeService.getByResetPasswordToken(token);
        model.addAttribute("token", token);
        if (employee == null) {
            model.addAttribute("message", "Invalid Token");
            return "message";
        }
        return RESET_PASSWORD_TEMPLATE;
    }

    @PostMapping(RESET_PASSWORD_URL)
    public String processResetPassword(HttpServletRequest request, Model model) {
        String token = request.getParameter("token");
        String password = request.getParameter("password");
        Employee employee = employeeService.getByResetPasswordToken(token);
        model.addAttribute("title", "Reset your password");
        if (employee == null) {
            model.addAttribute("message", "Invalid Token");
            return MESSAGE_TEMPLATE;
        } else {
            employeeService.updatePassword(employee, password);
            model.addAttribute("message", "You have successfully changed your password.");
        }
        return MESSAGE_TEMPLATE;
    }
}
