package com.bapemployeemanagement.capstoneproject.controller;

import com.bapemployeemanagement.capstoneproject.entity.Employee;
import com.bapemployeemanagement.capstoneproject.entity.dto.EmployeeDto;
import com.bapemployeemanagement.capstoneproject.service.EmployeeService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class UpdateController extends BaseController {
    /**
     * URLs
     */
    private static final String EDIT_URL = "/admin/edit";
    private static final String EDIT_SUCCESSFUL_URL = "/admin/edit-successful";
    private static final String ADMIN_URL = "/admin";

    /**
     * Templates
     */
    private static final String EDIT_PAGE_TEMPLATE = "edit_page";

    private final EmployeeService employeeService;

    public UpdateController(EmployeeService employeeService) {
        super(employeeService);
        this.employeeService = employeeService;
    }

    @GetMapping(EDIT_URL)
    public String updatePage(Model model, Principal principal) {
        Employee employee = getEmployee(principal);
        EmployeeDto employeeDto = new EmployeeDto();
        BeanUtils.copyProperties(employee, employeeDto);
        Date dob = employee.getEmployeeDob();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dobString = dateFormat.format(dob);
        employeeDto.setEmployeeDob(dobString);
        model.addAttribute("employeeDto", employeeDto);
        return EDIT_PAGE_TEMPLATE;
    }

    @PostMapping(EDIT_SUCCESSFUL_URL)
    public String updateProcess(@ModelAttribute("employeeDto") EmployeeDto employeeDto) throws ParseException {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeDto, employee);
        String dobString = employeeDto.getEmployeeDob();
        Date dob = new SimpleDateFormat("yyyy-MM-dd").parse(dobString);
        employee.setEmployeeDob(dob);
        employeeService.updateEmployee(employee);
        return "redirect:" + ADMIN_URL;
    }
}
