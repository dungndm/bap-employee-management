package com.bapemployeemanagement.capstoneproject.controller;

import com.bapemployeemanagement.capstoneproject.entity.Employee;
import com.bapemployeemanagement.capstoneproject.entity.dto.EmployeeDto;
import com.bapemployeemanagement.capstoneproject.service.EmailService;
import com.bapemployeemanagement.capstoneproject.service.EmployeeService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.repository.query.Param;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class VerifyEmailController extends BaseController {
    /**
     * URLs
     */
    private static final String REGISTER_URL = "/register";
    private static final String PROCESS_REGISTER_URL = "/process_register";
    private static final String VERIFY_URL = "/verify";

    /**
     * Templates
     */
    private static final String REGISTER_PAGE_TEMPLATE = "register_employee";
    private static final String REGISTER_SUCCESS_PAGE_TEMPLATE = "register_success";
    private static final String VERIFY_SUCCESS_PAGE_TEMPLATE = "verify_success";
    private static final String VERIFY_FAIL_PAGE_TEMPLATE = "verify_fail";

    private final EmailService emailService;
    private final EmployeeService employeeService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public VerifyEmailController(EmailService emailService,
                                 EmployeeService employeeService,
                                 BCryptPasswordEncoder bCryptPasswordEncoder) {
        super(employeeService);
        this.emailService = emailService;
        this.employeeService = employeeService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @GetMapping(REGISTER_URL)
    public String registerEmployee(Model model) {
        model.addAttribute("employeeRegister", new EmployeeDto());
        return REGISTER_PAGE_TEMPLATE;
    }

    @PostMapping(PROCESS_REGISTER_URL)
    public String processRegister(@ModelAttribute("employeeRegister") EmployeeDto employeeDto, HttpServletRequest request) throws ParseException {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeDto, employee);
        String dobString = employeeDto.getEmployeeDob();
        Date dob = new SimpleDateFormat("yyyy-MM-dd").parse(dobString);
        employee.setEmployeeDob(dob);
        String password = employeeDto.getPassword();
        String encryptedPassword = bCryptPasswordEncoder.encode(password);
        employee.setEncryptedPassword(encryptedPassword);
        employeeService.register(employee, getSiteURL(request));
        emailService.sendEmail(employee, getSiteURL(request));
        return REGISTER_SUCCESS_PAGE_TEMPLATE;
    }

    @GetMapping(VERIFY_URL)
    public String verifyUser(@Param("code") String code) {
        if (employeeService.verify(code)) {
            return VERIFY_SUCCESS_PAGE_TEMPLATE;
        } else {
            return VERIFY_FAIL_PAGE_TEMPLATE;
        }
    }
}
