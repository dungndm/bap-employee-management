package com.bapemployeemanagement.capstoneproject.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "employee")
@Getter
@Setter
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employee_id", nullable = false)
    private Integer employeeId;

    @Column(name = "employee_name", columnDefinition = "nvarchar(150)", nullable = false)
    private String employeeName;

    private boolean gender;

    @Column(name = "dob", nullable = false)
    private Date employeeDob;

    @Column(name = "code_language", columnDefinition = "varchar(36)")
    private String codeLanguage;

    @Column(name = "site_office", columnDefinition = "varchar(36)")
    private String siteOffice;

    @Column(columnDefinition = "varchar(500)", nullable = false)
    private String email;

    @Column(name = "encrypted_password", columnDefinition = "varchar(1000)", nullable = false)
    private String encryptedPassword;

    @Column(name = "verification_code", columnDefinition = "varchar(500)")
    private String verificationCode;

    private boolean enabled;

    @Column(name = "reset_password_token", columnDefinition = "varchar(30)")
    private String resetPasswordToken;

    @Column(name = "employee_role", columnDefinition = "varchar(36)")
    private String employeeRole;
}
