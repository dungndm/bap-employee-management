package com.bapemployeemanagement.capstoneproject.entity.dto;

import com.bapemployeemanagement.capstoneproject.entity.Employee;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AttendanceDto {
    private Integer attendanceId;
    private Employee employee;
    private String checkIn;
    private String checkOut;
}
