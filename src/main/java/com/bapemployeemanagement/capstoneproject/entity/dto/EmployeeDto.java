package com.bapemployeemanagement.capstoneproject.entity.dto;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class EmployeeDto {
    private Integer employeeId;
    private String employeeName;
    private boolean gender;
    private String employeeDob;
    private String codeLanguage;
    private String siteOffice;
    private String email;
    private String password;
    private String encryptedPassword;
    private String verificationCode;
    private boolean enabled;
    private String employeeRole;
}
