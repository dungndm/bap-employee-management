package com.bapemployeemanagement.capstoneproject.repository;

import com.bapemployeemanagement.capstoneproject.entity.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AttendanceRepository extends JpaRepository<Attendance, Integer> {

    @Query(value = "select * from employee_management.attendance " +
            "where attendance_id = (select max(attendance_id) from attendance)", nativeQuery = true)
    Optional<Attendance> getLatestAttendance();

    @Query(value = "select * from employee_management.attendance " +
            "where employee_id = :employeeId", nativeQuery = true)
    List<Attendance> findAllById(Integer employeeId);
}