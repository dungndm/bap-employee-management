package com.bapemployeemanagement.capstoneproject.repository;

import com.bapemployeemanagement.capstoneproject.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    Optional<Employee> findEmployeeByEmail(String email);

    @Query(value = "select e.employee_role from employee_management.employee e where e.employee_id = ?1", nativeQuery = true)
    String getRoleNameByEmployeeId(Integer employeeId);

    @Query(value = "select * from employee_management.employee e where e.verification_code = ?1", nativeQuery = true)
    Employee findByVerificationCode(String code);

    Employee findByResetPasswordToken(String token);
}
