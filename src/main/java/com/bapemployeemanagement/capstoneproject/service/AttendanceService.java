package com.bapemployeemanagement.capstoneproject.service;

import com.bapemployeemanagement.capstoneproject.entity.Attendance;
import com.bapemployeemanagement.capstoneproject.repository.AttendanceRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AttendanceService {

    private final AttendanceRepository attendanceRepository;

    public AttendanceService(AttendanceRepository attendanceRepository) {
        this.attendanceRepository = attendanceRepository;
    }

    public void saveAttendance(Attendance attendance) {
        attendanceRepository.save(attendance);
    }

    public Attendance getLatestAttendance() {
        Optional<Attendance> optionalAttendance = attendanceRepository.getLatestAttendance();
        if (optionalAttendance.isPresent()) {
            return optionalAttendance.get();
        }
        return null;
    }

    public List<Attendance> getAllAttendance(Integer employeeId) {
        return attendanceRepository.findAllById(employeeId);
    }
}
