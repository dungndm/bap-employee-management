package com.bapemployeemanagement.capstoneproject.service;

import com.bapemployeemanagement.capstoneproject.entity.Employee;
import com.bapemployeemanagement.capstoneproject.repository.EmployeeRepository;
import net.bytebuddy.utility.RandomString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class EmployeeService implements UserDetailsService {
    private final EmployeeRepository employeeRepository;
    private final EmailService emailService;

    public EmployeeService(EmployeeRepository employeeRepository, EmailService emailService) {
        this.employeeRepository = employeeRepository;
        this.emailService = emailService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Employee> optionalEmployee = employeeRepository.findEmployeeByEmail(email);
        if (!optionalEmployee.isPresent()) {
            throw new UsernameNotFoundException("Employee was not founded in database");
        }
        Employee employee = optionalEmployee.get();
        String roleName = employeeRepository.getRoleNameByEmployeeId(employee.getEmployeeId());
        List<GrantedAuthority> grantedList = new ArrayList<>();
        if (roleName != null) {
            GrantedAuthority authority = new SimpleGrantedAuthority(roleName);
            grantedList.add(authority);
        }
        return new User(employee.getEmail(), employee.getEncryptedPassword(), grantedList);
    }

    public void register(Employee employee, String siteURL) {
        String randomCode = RandomString.make(64);
        employee.setVerificationCode(randomCode);
        employee.setEnabled(false);
        employeeRepository.save(employee);
        emailService.sendEmail(employee, siteURL);
    }

    public boolean verify(String verificationCode) {
        Employee employee = employeeRepository.findByVerificationCode(verificationCode);
        if (employee == null || employee.isEnabled()) {
            return false;
        } else {
            employee.setVerificationCode(verificationCode);
            employee.setEnabled(true);
            employee.setEmployeeRole("ROLE_USER");
            employeeRepository.save(employee);
            return true;
        }
    }

    public void updateResetPasswordToken(String token, String email) throws UsernameNotFoundException {
        Optional<Employee> optionalEmployee = employeeRepository.findEmployeeByEmail(email);
        if (optionalEmployee.isPresent()) {
            Employee employee = optionalEmployee.get();
            employee.setResetPasswordToken(token);
            employeeRepository.save(employee);
        } else {
            throw new UsernameNotFoundException("Could not find any employee with the email " + email);
        }
    }

    public Employee getByResetPasswordToken(String token) {
        return employeeRepository.findByResetPasswordToken(token);
    }

    public void updatePassword(Employee employee, String newPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(newPassword);
        employee.setEncryptedPassword(encodedPassword);
        employee.setResetPasswordToken(null);
        employeeRepository.save(employee);
    }

    public Employee findEmployeeByEmail(String email) {
        Optional<Employee> optionalEmployee = employeeRepository.findEmployeeByEmail(email);
        if (optionalEmployee.isPresent()) {
            Employee employee = optionalEmployee.get();
            return employee;
        }
        return null;
    }

    public void updateEmployee(Employee employee) {
        employeeRepository.save(employee);
    }
}
